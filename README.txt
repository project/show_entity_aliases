INTRODUCTION
------------
Show Entity Aliases module enables privileged users to see, edit and delete
all url aliases (paths) assigned to an alias compatible entity, from within the entity edit page.

REQUIREMENTS
------------
This module requires the following modules to be enabled:
 * Path (https://www.drupal.org/documentation/modules/path)

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

MAINTAINERS
-----------
Current maintainers:
 * Artem Kolotilkin - https://drupal.org/user/1915462
